// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.arara.build

/**
 * The plugin and non-plugin versions used during arara's build.
 */
object Versions {
    // plugin dependencies
    const val detekt = "1.20.0"
    const val dokka = "1.8.10"
    const val kotlin = "1.8.20"
    const val shadow = "7.1.2"
    const val spotless = "6.5.2"
    const val spotlessChangelog = "2.4.0"
    const val versionsPlugin = "0.46.0"

    // non-plugin dependencies
    const val clikt = "3.4.2"
    const val coroutines = "1.6.1"
    const val jna = "5.13.0"
    const val junit = "5.9.2"
    const val kaml = "0.43.0"
    const val korlibs = "2.7.0"
    const val kotest = "4.6.3"
    const val kotlinLogging = "2.1.21"
    const val kotlinxSerialization = "1.5.0"
    const val jackson = "2.13.1"
    const val log4j = "2.20.0"
    const val luak = "2.4.13"
    const val mvel = "2.4.15.Final"
    const val slf4j = "1.7.36"
    const val yamlkt = "0.12.0"
    const val ztExec = "1.12"
}
